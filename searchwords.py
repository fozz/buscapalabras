import sys
import sortwords

def search_word(word, words_list):
    try:
        index = words_list.index(word)
        return index
    except ValueError:
        raise Exception("Palabra no encontrada en la lista")

def main():
    if len(sys.argv) < 3:
        print("Uso: ./searchwords.py <palabra_a_buscar> <lista_de_palabras>")
        sys.exit(1)

    search_word_arg = sys.argv[1]
    words_list_arg = sys.argv[2:]

    try:
        ordered_list = sortwords.sort(words_list_arg)

        sortwords.show(ordered_list)

        position = search_word(search_word_arg, ordered_list)
        print(position)
    except Exception as e:
        print(f"Error: {e}")
        sys.exit(1)

if __name__ == '__main__':
    main()
